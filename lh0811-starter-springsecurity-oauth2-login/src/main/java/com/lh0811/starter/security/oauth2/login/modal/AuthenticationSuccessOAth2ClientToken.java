package com.lh0811.starter.security.oauth2.login.modal;

import com.lh0811.starter.security.common.modal.SystemUser;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;

import java.util.Collection;

public class AuthenticationSuccessOAth2ClientToken extends AbstractAuthenticationToken {

    @Getter
    @Setter
    private String token;

    @Getter
    @Setter
    private Boolean anonymousFlag;

    private AuthenticationSuccessOAth2ClientToken(Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
    }

    // 认证成功后
    public static AuthenticationSuccessOAth2ClientToken buildForOAuth2AuthenticationToken(OAuth2AuthenticationToken oAuth2AuthenticationToken) {
        AuthenticationSuccessOAth2ClientToken authenticationSuccessToken = new AuthenticationSuccessOAth2ClientToken(oAuth2AuthenticationToken.getAuthorities());
        // 设置为已认证状态
        authenticationSuccessToken.setAuthenticated(Boolean.TRUE);
        authenticationSuccessToken.setAnonymousFlag(Boolean.FALSE);
        // 用户详情为MyUser
        authenticationSuccessToken.setDetails(oAuth2AuthenticationToken);
        return authenticationSuccessToken;
    }

    // 这个AuthenticationToken用于做系统中通过各种认证方式认证后的AuthenticationToken，不必返回凭证
    @Override
    public String getCredentials() {
        return "";
    }

    // 这个AuthenticationToken用于做系统中通过各种认证方式认证后的AuthenticationToken，主信息直接用UserId就可以。
    @Override
    public String getPrincipal() {
        return ((SystemUser)this.getDetails()).getUserId();
    }


}