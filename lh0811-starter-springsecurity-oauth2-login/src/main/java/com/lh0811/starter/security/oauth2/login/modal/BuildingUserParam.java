package com.lh0811.starter.security.oauth2.login.modal;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BuildingUserParam {
    @NotBlank(message = "平台id不能为空")
    private String registrationId;
    @NotBlank(message = "三方认证平用户id不能为空")
    private String thirdUserId;
    @NotBlank(message = "用户手机号不能为空")
    private String phone;
    @NotBlank(message = "用户密码不能为空")
    private String pwd;
    @NotBlank(message = "确认密码不能为空")
    private String repwd;
}