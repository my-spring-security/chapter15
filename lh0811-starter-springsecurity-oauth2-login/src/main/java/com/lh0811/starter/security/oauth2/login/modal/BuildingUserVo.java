package com.lh0811.starter.security.oauth2.login.modal;

import com.lh0811.starter.security.common.modal.SystemUser;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BuildingUserVo {
    @NotBlank(message = "0-新用户绑定成功(需不补充信息) 1-已存在用户绑定信息 2-绑定失败 ")
    private String result;
    @NotBlank(message = "用户信息")
    private SystemUser systemUser;
}