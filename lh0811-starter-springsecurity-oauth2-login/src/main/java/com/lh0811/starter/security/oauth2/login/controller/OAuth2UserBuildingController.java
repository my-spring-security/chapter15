package com.lh0811.starter.security.oauth2.login.controller;

import com.lh0811.starter.security.common.modal.AuthenticationSuccessToken;
import com.lh0811.starter.security.component.token.TokenManager;
import com.lh0811.starter.security.oauth2.login.component.OAuth2ThirdUserAndLocalUserRelManager;
import com.lh0811.starter.security.oauth2.login.modal.BuildingUserParam;
import com.lh0811.starter.security.oauth2.login.modal.BuildingUserVo;
import com.lh0811.starter.web.response.ServerResponse;
import com.lh0811.starter.web.response.TokenVo;
import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OAuth2UserBuildingController {

    @Resource
    private OAuth2ThirdUserAndLocalUserRelManager oAuth2ThirdUserAndLocalUserRelManager;

    @Resource
    private TokenManager tokenManager;


    @PostMapping("${lh0811.security.oauth2.login.userBuildingProgressUrl:/oauth2/login/user_building}")
    public ServerResponse buildingUser(@RequestBody @Valid BuildingUserParam param) throws Exception {
        BuildingUserVo buildingUserVo = oAuth2ThirdUserAndLocalUserRelManager.buildingUser(param);
        AuthenticationSuccessToken authenticationSuccessToken = AuthenticationSuccessToken.buildForSystemUser(buildingUserVo.getSystemUser());
        String token = tokenManager.generateToken(authenticationSuccessToken);
        return ServerResponse.createBySuccess("用户绑定成功",new TokenVo(token));
    }

}
