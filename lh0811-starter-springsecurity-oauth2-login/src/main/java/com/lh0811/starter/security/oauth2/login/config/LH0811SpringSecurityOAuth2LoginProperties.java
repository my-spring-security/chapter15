package com.lh0811.starter.security.oauth2.login.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties(prefix = "lh0811.security.oauth2.login")
public class LH0811SpringSecurityOAuth2LoginProperties {

    // 是否启用OAuth2.0 client 认证登录
    private Boolean enableOAuth2Login = false;

    // OAuth2.0 第三方认证， 引导用户填写账号绑定信息的页面地址
    private String userBuildingPageUrl =  "/building-user.html";

    // OAuth2.0 第三方认证，用绑定的url
    private String userBuildingProgressUrl = "/oauth2/login/user_building";


}
