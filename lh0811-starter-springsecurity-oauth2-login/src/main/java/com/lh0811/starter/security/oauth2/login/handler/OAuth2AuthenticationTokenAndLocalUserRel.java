package com.lh0811.starter.security.oauth2.login.handler;


public interface OAuth2AuthenticationTokenAndLocalUserRel {


    String getLocalUserId();// 本地用户id
    String getRegistrationId();// 三方认证平台id
    String getThirdUserId();// 三方认证平台用户id
    Boolean getBuildingFlag();// 是否绑定用户

}