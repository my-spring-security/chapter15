package com.lh0811.starter.security.oauth2.login.component.impl;

import com.lh0811.starter.security.common.modal.SystemUser;
import com.lh0811.starter.security.oauth2.login.component.OAuth2ThirdUserAndLocalUserRelManager;
import com.lh0811.starter.security.oauth2.login.modal.BuildingUserParam;
import com.lh0811.starter.security.oauth2.login.modal.BuildingUserVo;
import com.lh0811.starter.web.exception.CommonException;
import com.lh0811.starter.web.response.ServerResponse;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DefaultOAuth2ThirdUserAndLocalUserRelManagerImpl implements OAuth2ThirdUserAndLocalUserRelManager {

    @Override
    public SystemUser loadUser(String registrationId, String thirdId) throws Exception {
        throw  CommonException.create(ServerResponse.createByError("必须自定义实现"));
    }

    @Override
    public BuildingUserVo buildingUser(BuildingUserParam param) throws Exception {
        throw  CommonException.create(ServerResponse.createByError("必须自定义实现"));
    }


}
