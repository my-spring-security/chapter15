package com.lh0811.starter.security.oauth2.login;

import com.lh0811.starter.security.common.filter.TokenSecurityContextHolderFilter;
import com.lh0811.starter.security.common.handler.JsonAccessDeniedHandler;
import com.lh0811.starter.security.common.handler.JsonAuthenticationEntryPoint;
import com.lh0811.starter.security.common.handler.JsonLogoutSuccessHandler;
import com.lh0811.starter.security.component.BaseAbstractAuthenticationProcessingFilter;
import com.lh0811.starter.security.component.BaseAuthenticationConfig;
import com.lh0811.starter.security.component.BaseAuthenticationProvider;
import com.lh0811.starter.security.component.token.TokenManager;
import com.lh0811.starter.security.config.LH0811SpringSecurityProperties;
import com.lh0811.starter.security.oauth2.login.component.OAuth2ThirdUserAndLocalUserRelManager;
import com.lh0811.starter.security.oauth2.login.config.LH0811SpringSecurityOAuth2LoginProperties;
import com.lh0811.starter.security.oauth2.login.handler.OAuth2ClientJsonAuthenticationFailureHandler;
import com.lh0811.starter.security.oauth2.login.handler.OAuth2ClientJsonAuthenticationSuccessHandler;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.context.SecurityContextHolderFilter;

import java.util.List;

@EnableConfigurationProperties({LH0811SpringSecurityOAuth2LoginProperties.class})
@ComponentScan("com.lh0811.starter.security.oauth2.login.controller")
public class LH0811StarterSpringSecurityOAuth2LoginConfigure {
    private TokenManager tokenManager;

    private List<BaseAuthenticationProvider> providerList;

    private OAuth2ThirdUserAndLocalUserRelManager oAuth2ThirdUserAndLocalUserRelManager;

    private LH0811SpringSecurityProperties lh0811SpringSecurityProperties;

    private LH0811SpringSecurityOAuth2LoginProperties lh0811SpringSecurityOAuth2LoginProperties;


    public LH0811StarterSpringSecurityOAuth2LoginConfigure(
            @Lazy List<BaseAuthenticationProvider> providerList,
            @Lazy TokenManager tokenManager,
            @Lazy LH0811SpringSecurityProperties lh0811SpringSecurityProperties,
            @Lazy LH0811SpringSecurityOAuth2LoginProperties lh0811SpringSecurityOAuth2LoginProperties,
            @Lazy OAuth2ThirdUserAndLocalUserRelManager oAuth2ThirdUserAndLocalUserRelManager
    ) {
        this.providerList = providerList;
        this.tokenManager = tokenManager;
        this.lh0811SpringSecurityProperties = lh0811SpringSecurityProperties;
        this.lh0811SpringSecurityOAuth2LoginProperties = lh0811SpringSecurityOAuth2LoginProperties;
        this.oAuth2ThirdUserAndLocalUserRelManager = oAuth2ThirdUserAndLocalUserRelManager;
    }


    @Bean
    @Primary
    SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http) throws Exception {
        // 所有请求都需要认证后访问
        http.authorizeHttpRequests((authorizeHttpRequests) -> authorizeHttpRequests.anyRequest().authenticated());
        // 禁用默认的formLogin
        http.formLogin((formLogin) -> formLogin.disable());
        // 禁用httpBasic
        http.httpBasic((httpBasic) -> httpBasic.disable());
        // 禁用csrf
        http.csrf((csrf) -> csrf.disable());
        // 判断是否启用OAuth2.0 client
        if (BooleanUtils.isTrue(lh0811SpringSecurityOAuth2LoginProperties.getEnableOAuth2Login())) {
            // 启用Oauth2Client 认证
            http.oauth2Login((oauth2) -> oauth2
                    .failureHandler(new OAuth2ClientJsonAuthenticationFailureHandler())
                    .successHandler(new OAuth2ClientJsonAuthenticationSuccessHandler(oAuth2ThirdUserAndLocalUserRelManager,tokenManager, lh0811SpringSecurityOAuth2LoginProperties))
            );
        }

        // 将token解析认证信息的Filter 添加到SecurityContextHolderFilter之后
        http.addFilterAfter(new TokenSecurityContextHolderFilter(tokenManager,lh0811SpringSecurityProperties), SecurityContextHolderFilter.class);

        // 启用自定义认证流程
        BaseAbstractAuthenticationProcessingFilter filter = new BaseAbstractAuthenticationProcessingFilter(lh0811SpringSecurityProperties);
        http.apply(new BaseAuthenticationConfig<>(filter,providerList,lh0811SpringSecurityProperties));

        // 注销
        http.logout((logout) -> logout
                .logoutUrl(lh0811SpringSecurityProperties.getLogoutProcessingUrl())
                .logoutSuccessHandler(new JsonLogoutSuccessHandler(tokenManager)));
        // 异常
        http.exceptionHandling((exception) -> exception
                // 用户未认证异常处理。 未认证的情况下默认是LoginUrlAuthenticationEntryPoint 跳转到 请求用户认证的页面
                .authenticationEntryPoint(new JsonAuthenticationEntryPoint())
                // 鉴权失败异常处理。
                .accessDeniedHandler(new JsonAccessDeniedHandler())
        );
        return http.build();
    }


}
