package com.lh0811.starter.security.oauth2.login.handler;

import com.alibaba.fastjson2.JSON;
import com.lh0811.starter.security.common.modal.AuthenticationSuccessToken;
import com.lh0811.starter.security.common.modal.SystemUser;
import com.lh0811.starter.security.component.token.TokenManager;
import com.lh0811.starter.security.oauth2.login.component.OAuth2ThirdUserAndLocalUserRelManager;
import com.lh0811.starter.security.oauth2.login.config.LH0811SpringSecurityOAuth2LoginProperties;
import com.lh0811.starter.web.response.ServerResponse;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import java.io.IOException;

@Slf4j
public class OAuth2ClientJsonAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    private OAuth2ThirdUserAndLocalUserRelManager userRelManager;
    private TokenManager tokenManager;

    private LH0811SpringSecurityOAuth2LoginProperties lh0811SpringSecurityOAuth2LoginProperties;

    public OAuth2ClientJsonAuthenticationSuccessHandler(OAuth2ThirdUserAndLocalUserRelManager userRelManager, TokenManager tokenManager, LH0811SpringSecurityOAuth2LoginProperties lh0811SpringSecurityOAuth2LoginProperties) {
        this.userRelManager = userRelManager;
        this.tokenManager = tokenManager;
        this.lh0811SpringSecurityOAuth2LoginProperties = lh0811SpringSecurityOAuth2LoginProperties;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        response.setContentType("application/json;charset=utf-8");
        response.setStatus(HttpStatus.OK.value());
        try {
            // 获取到OAuth2 三方认证后的信息OAuth2AuthenticationToken
            OAuth2AuthenticationToken oAuth2AuthenticationToken = (OAuth2AuthenticationToken) authentication;

            // 通过OAuth2AuthenticationToken 来判断是否已经绑定了本地用户
            SystemUser systemUser = userRelManager.loadUser(oAuth2AuthenticationToken.getAuthorizedClientRegistrationId(),oAuth2AuthenticationToken.getName());
            // 未绑定本地用户
            if(systemUser == null) {
                // 响应给客户端
                // 平台id
                response.addCookie(new Cookie("registrationId",oAuth2AuthenticationToken.getAuthorizedClientRegistrationId()));
                // 平台用户id
                response.addCookie(new Cookie("thirdUserId",oAuth2AuthenticationToken.getName()));
                // 本地用户绑定url
                response.addCookie(new Cookie("buildingUrl", lh0811SpringSecurityOAuth2LoginProperties.getUserBuildingProgressUrl()));
                // 转发到用户绑定页面，引导用户输入信息并提交到buildingUrl
                request.getRequestDispatcher(lh0811SpringSecurityOAuth2LoginProperties.getUserBuildingPageUrl()).forward(request,response);
            }
            // 已经绑定用户
            else {
                // 构建一个已认证的AuthenticationSuccessToken
                AuthenticationSuccessToken authenticationSuccessToken = AuthenticationSuccessToken.buildForSystemUser(systemUser);
                // 创建Token
                String token = tokenManager.generateToken(authenticationSuccessToken);
                // 组装对客户端响应信息
                ServerResponse<String> serverResponse = ServerResponse.createBySuccess("登录成功", token);
                // 响应给客户端
                response.getWriter().write(JSON.toJSONString(serverResponse));
            }
        } catch (Exception e) {
            // 组装对客户端响应信息
            log.error("生成用户口令异常", e);
            // 响应给客户端
            response.getWriter().write(JSON.toJSONString(ServerResponse.createByError("登录失败,生成用户口令异常")));
        }
    }

}
