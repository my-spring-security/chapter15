package com.lh0811.starter.security.oauth2.login.component;


import com.lh0811.starter.security.common.modal.SystemUser;
import com.lh0811.starter.security.oauth2.login.modal.BuildingUserParam;
import com.lh0811.starter.security.oauth2.login.modal.BuildingUserVo;

public interface OAuth2ThirdUserAndLocalUserRelManager {


    SystemUser loadUser(String registrationId, String thirdId) throws Exception;

    BuildingUserVo buildingUser(BuildingUserParam param) throws Exception;
}
