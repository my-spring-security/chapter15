package com.lh0811.starter.security.component.token;


import org.springframework.security.core.Authentication;

/**
 * token 管理
 */
public interface TokenManager {

    /**
     * 保存 用户 token
     *
     * @param authentication
     */
    String generateToken(Authentication authentication);

    /**
     * 移除token
     *
     * @param token
     */
    void removeToken(String token);

    /**
     * 根据用户token 获取用户信息
     *
     * @param token
     * @return
     */
    Authentication getAuthenticationByToken(String token);

    /**
     * token 续期策略
     *
     * @param token
     */
    void delayExpired(String token);


    /**
     * 刷新token
     *
     * @return
     */
    String refreshToken(String string);

}
