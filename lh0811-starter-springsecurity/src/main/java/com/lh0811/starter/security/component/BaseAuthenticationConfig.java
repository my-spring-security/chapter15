package com.lh0811.starter.security.component;

import com.lh0811.starter.security.common.handler.JsonAuthenticationFailureHandler;
import com.lh0811.starter.security.common.handler.JsonAuthenticationSuccessHandler;
import com.lh0811.starter.security.config.LH0811SpringSecurityProperties;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.HttpSecurityBuilder;
import org.springframework.security.config.annotation.web.configurers.AbstractAuthenticationFilterConfigurer;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

import java.util.List;

/**
 * 自定义基础认证方式的Config类
 *
 * 主要的功能：
 * 1. 添加Token认证的AuthenticationProvider
 * 2. 为AbstractAuthenticationProcessingFilter设置 AuthenticationManager
 * 3. 为AbstractAuthenticationProcessingFilter设置 Handler
 * 4. AbstractAuthenticationProcessingFilter添加到过滤器链中
 */
public class BaseAuthenticationConfig<F extends BaseAbstractAuthenticationProcessingFilter,H extends HttpSecurityBuilder<H>> extends AbstractAuthenticationFilterConfigurer<H, BaseAuthenticationConfig<F,H>, F> {

    private F filter;

    private List<BaseAuthenticationProvider> providerList;

    private LH0811SpringSecurityProperties lh0811SpringSecurityProperties;

    // 构造方法
    public BaseAuthenticationConfig(F filter, List<BaseAuthenticationProvider> providerList,LH0811SpringSecurityProperties lh0811SpringSecurityProperties) {
        super(filter, lh0811SpringSecurityProperties.getLoginProcessingUrl());
        this.filter = filter;
        this.providerList = providerList;
    }

    // 配置认证URL的RequestMatcher
    protected RequestMatcher createLoginProcessingUrlMatcher(String loginProcessingUrl) {
        return new AntPathRequestMatcher(loginProcessingUrl, "POST");
    }

    @Override
    public void configure(H http) throws Exception {
        // 获取filter
        F authenticationFilter = this.getAuthenticationFilter();
        // 设置Filter的AuthenticationManager
        authenticationFilter.setAuthenticationManager(http.getSharedObject(AuthenticationManager.class));

        // 增加自定义的MyUsernamePasswordAuthenticationProvider
        for (AuthenticationProvider authenticationProvider : providerList) {
            http.authenticationProvider(authenticationProvider);
        }

        // 设置Filter的认证后Handler
        authenticationFilter.setAuthenticationSuccessHandler(new JsonAuthenticationSuccessHandler());
        authenticationFilter.setAuthenticationFailureHandler(new JsonAuthenticationFailureHandler());

        // 将filter添加到过滤器链中 SecurityFilterChain,放到UsernamePasswordAuthenticationFilter后面
        http.addFilterAfter(authenticationFilter, UsernamePasswordAuthenticationFilter.class);
    }

}
