package com.lh0811.starter.security.common.modal;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class AuthenticationSuccessToken extends AbstractAuthenticationToken {

    @Getter
    @Setter
    private String token;

    @Getter
    @Setter
    private Boolean anonymousFlag;

    private AuthenticationSuccessToken(Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
    }

    // 认证成功后
    public static AuthenticationSuccessToken buildForSystemUser(SystemUser systemUser) {
        AuthenticationSuccessToken authenticationToken = new AuthenticationSuccessToken(systemUser.getAuthorities());
        // 设置为已认证状态
        authenticationToken.setAuthenticated(Boolean.TRUE);
        authenticationToken.setAnonymousFlag(Boolean.FALSE);
        // 用户详情为MyUser
        authenticationToken.setDetails(systemUser);
        return authenticationToken;
    }

//    // 认证成功后
//    public static AuthenticationSuccessToken buildForOAuth2AuthenticationToken(OAuth2AuthenticationToken oAuth2AuthenticationToken) {
//        AuthenticationSuccessToken authenticationSuccessToken = new AuthenticationSuccessToken(oAuth2AuthenticationToken.getAuthorities());
//        // 设置为已认证状态
//        authenticationSuccessToken.setAuthenticated(Boolean.TRUE);
//        authenticationSuccessToken.setAnonymousFlag(Boolean.FALSE);
//        // 用户详情为MyUser
//        authenticationSuccessToken.setDetails(oAuth2AuthenticationToken);
//        return authenticationSuccessToken;
//    }

    // 这个AuthenticationToken用于做系统中通过各种认证方式认证后的AuthenticationToken，不必返回凭证
    @Override
    public String getCredentials() {
        return "";
    }

    // 这个AuthenticationToken用于做系统中通过各种认证方式认证后的AuthenticationToken，主信息直接用UserId就可以。
    @Override
    public String getPrincipal() {
        return ((SystemUser)this.getDetails()).getUserId();
    }


}