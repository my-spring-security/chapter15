package com.lh0811.starter.security.common.handler;

import com.alibaba.fastjson2.JSON;
import com.lh0811.starter.web.response.ResponseCode;
import com.lh0811.starter.web.response.ServerResponse;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import java.io.IOException;

@Slf4j
public class JsonAccessDeniedHandler implements AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        response.setContentType("application/json;charset=utf-8");
        response.setStatus(HttpStatus.OK.value());
        // 组装对客户端响应信息
        ServerResponse<String> serverResponse = ServerResponse.createByError(ResponseCode.ERR_AUTH.getCode(), "当前用户没有权限访问该资源,请联系管理员申请资源权限。");
        // 响应给客户端
        response.getWriter().write(JSON.toJSONString(serverResponse));
    }
}
