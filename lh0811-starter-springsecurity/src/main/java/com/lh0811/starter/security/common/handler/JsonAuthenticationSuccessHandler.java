package com.lh0811.starter.security.common.handler;

import com.alibaba.fastjson2.JSON;
import com.lh0811.starter.security.common.modal.SystemUser;
import com.lh0811.starter.web.response.ServerResponse;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import java.io.IOException;

@Slf4j
public class JsonAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        response.setContentType("application/json;charset=utf-8");
        response.setStatus(HttpStatus.OK.value());
        try {
            SystemUser myUser = (SystemUser) authentication.getDetails();
            // 组装对客户端响应信息
            ServerResponse<String> serverResponse = ServerResponse.createBySuccess("登录成功", myUser.getToken());
            // 响应给客户端
            response.getWriter().write(JSON.toJSONString(serverResponse));
        } catch (Exception e) {
            // 组装对客户端响应信息
            log.error("生成用户口令异常", e);
            // 响应给客户端
            response.getWriter().write(JSON.toJSONString(ServerResponse.createByError("登录失败,生成用户口令异常")));
        }
    }
}
