package com.lh0811.starter.security.component.token.impl;

import com.alibaba.fastjson2.JSON;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalListener;
import com.lh0811.starter.security.component.token.TokenManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Slf4j
public class DefaultTokenManager implements TokenManager {

    // 创建一个 缓存，并定义缓存超时策略为 最后一次访问后超过15分钟后过期
    private static final Cache<String, Authentication> TokenCache = CacheBuilder.newBuilder()
            .expireAfterWrite(15, TimeUnit.MINUTES)
            .expireAfterAccess(15, TimeUnit.MINUTES)
            .removalListener((RemovalListener<String, Authentication>) notification -> log.info(notification.getKey() + ":" + notification.getCause()))
            .build();

    @Override
    public String generateToken(Authentication authentication) {
        // 生成token
        String token = UUID.randomUUID().toString();
        // 将token 与 authentication 通过缓存建立关联
        TokenCache.put(token, authentication);
        // 返回token
        return token;
    }

    @Override
    public void removeToken(String token) {
        // 失效缓存数据
        TokenCache.invalidate(token);
    }

    @Override
    public Authentication getAuthenticationByToken(String token) {
        // 通过token 获取到用户认证信息
        Authentication authentication = TokenCache.getIfPresent(token);
        log.info("当前用户信息: {}", JSON.toJSONString(authentication));
        return authentication;
    }

    @Override
    public void delayExpired(String token) {
        // 这里不需要特意去做续期，因为 缓存超时策略为 最后一次访问后超过15分钟后过期 这里只需要读取一次数据就行了
        Authentication authentication = TokenCache.getIfPresent(token);
        log.info("当前用户信息: {}", JSON.toJSONString(authentication));
    }

    @Override
    public String refreshToken(String token) {
        Authentication authenticationByToken = getAuthenticationByToken(token);
        if (authenticationByToken == null) {
            throw new BadCredentialsException("Token无效，无法刷新新token");
        }
        return generateToken(authenticationByToken);
    }
}
