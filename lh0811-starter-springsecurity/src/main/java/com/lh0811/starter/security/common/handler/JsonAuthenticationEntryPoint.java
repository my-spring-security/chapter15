package com.lh0811.starter.security.common.handler;

import com.alibaba.fastjson2.JSON;
import com.lh0811.starter.web.response.ResponseCode;
import com.lh0811.starter.web.response.ServerResponse;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import java.io.IOException;

@Slf4j
public class JsonAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        response.setContentType("application/json;charset=utf-8");
        response.setStatus(HttpStatus.OK.value());
        // 组装对客户端响应信息
        ServerResponse<String> serverResponse = ServerResponse.createByError(ResponseCode.NO_AUTH.getCode(), "未获取到用户信息，请登录后重试。");
        // 响应给客户端
        response.getWriter().write(JSON.toJSONString(serverResponse));
    }
}
