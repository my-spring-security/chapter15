package com.lh0811.starter.security.component;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;

import java.io.IOException;

public interface BaseAuthenticationProvider extends AuthenticationProvider {

    // 确认登录方式
    String loginType();

    // 从Request中获取用户认证信息
    Authentication genAuthenticationFromRequest(HttpServletRequest request) throws IOException;

}
