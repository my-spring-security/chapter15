package com.lh0811.starter.security.common.modal;

import org.springframework.security.core.userdetails.UserDetails;

public interface SystemUser extends UserDetails {


    /**
     * 获取用户id
     * @return
     */
    String getUserId();

    /**
     * 要求系统用户在认证后，可以提供用户token
     */
    String getToken();

}
