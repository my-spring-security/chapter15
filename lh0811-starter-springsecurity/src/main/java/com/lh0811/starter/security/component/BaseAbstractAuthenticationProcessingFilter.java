package com.lh0811.starter.security.component;

import com.lh0811.starter.security.config.LH0811SpringSecurityProperties;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import java.io.IOException;

public class BaseAbstractAuthenticationProcessingFilter extends AbstractAuthenticationProcessingFilter {

    private LH0811SpringSecurityProperties lh0811SpringSecurityProperties;

    public BaseAbstractAuthenticationProcessingFilter(LH0811SpringSecurityProperties lh0811SpringSecurityProperties) {
        super(lh0811SpringSecurityProperties.getLoginProcessingUrl());
        this.lh0811SpringSecurityProperties = lh0811SpringSecurityProperties;
    }

    // 根据不同的登录方式，来调用不同的AuthenticationProvider从Request中获取Authentication
    // 调用AuthenticationManager认证Authentication
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
        request.setCharacterEncoding("UTF8");
        response.setContentType("application/json;charset=utf-8");
        response.setStatus(HttpStatus.OK.value());
        if (this.getAuthenticationManager() instanceof ProviderManager) {
            ProviderManager providerManager = (ProviderManager) this.getAuthenticationManager();

            // 获取认证类型
            String loginType = request.getParameter(lh0811SpringSecurityProperties.getLoginTypeKey());
            BaseAuthenticationProvider provider = null;
            for (AuthenticationProvider providerManagerProvider : providerManager.getProviders()) {
                if (providerManagerProvider instanceof BaseAuthenticationProvider) {
                    if (StringUtils.equals(loginType, ((BaseAuthenticationProvider) providerManagerProvider).loginType())) {
                        provider = (BaseAuthenticationProvider) providerManagerProvider;
                        break;
                    }
                }
            }

            if (provider == null) {
                throw new BadCredentialsException("未上传登录类型");
            }

            // 获取认证凭证
            Authentication authentication = provider.genAuthenticationFromRequest(request);

            try {
                return this.getAuthenticationManager().authenticate(authentication);
            }catch (Exception e) {
                if (e instanceof BadCredentialsException) {
                    throw e;
                }
                throw new BadCredentialsException("加载用户信息失败,请检查系统配置。");
            }
        }else {
            throw new BadCredentialsException("AuthenticationManager配置错误，非默认的ProviderManager");
        }
    }

}
