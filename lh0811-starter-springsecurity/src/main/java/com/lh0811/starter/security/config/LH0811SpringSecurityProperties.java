package com.lh0811.starter.security.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties(prefix = "lh0811.security")
public class LH0811SpringSecurityProperties {

    // 认证接口URL
    private String loginProcessingUrl = "/login";

    // 注销接口URL
    private String logoutProcessingUrl = "/logout";

    // 认证请求中认证类型的Type对应的QueryParam key
    private String loginTypeKey = "type";

    private String tokenKey = "Authorization";

    // 角色权限前缀
    private String rolePrefix = "ROLE_";

}
