package com.lh0811.starter.security.common.filter;

import com.lh0811.starter.security.component.token.TokenManager;
import com.lh0811.starter.security.config.LH0811SpringSecurityProperties;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextHolderStrategy;
import org.springframework.web.filter.GenericFilterBean;

import java.io.IOException;

@Getter
@Setter
public class TokenSecurityContextHolderFilter extends GenericFilterBean {

    private static final String FILTER_APPLIED = TokenSecurityContextHolderFilter.class.getName() + ".APPLIED";
    private SecurityContextHolderStrategy securityContextHolderStrategy = SecurityContextHolder.getContextHolderStrategy();
    private TokenManager tokenManager;

    private LH0811SpringSecurityProperties lh0811SpringSecurityProperties;

    public TokenSecurityContextHolderFilter(TokenManager tokenManager,LH0811SpringSecurityProperties lh0811SpringSecurityProperties) {
        this.tokenManager = tokenManager;
        this.lh0811SpringSecurityProperties = lh0811SpringSecurityProperties;
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        this.doFilter((HttpServletRequest) request, (HttpServletResponse) response, chain);
    }

    private void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        if (request.getAttribute(FILTER_APPLIED) != null) {
            chain.doFilter(request, response);
        } else {
            // 设置 当前过滤器
            request.setAttribute(FILTER_APPLIED, Boolean.TRUE);
            // 从请求中获取上传的Token
            String reqToken = request.getHeader(lh0811SpringSecurityProperties.getTokenKey());
            // 未获取到上传的Token
            if (StringUtils.isBlank(reqToken)) {
                // 继续执行后续过滤器
                chain.doFilter(request, response);
                return;
            }

            // 通过token获取用户认证信息
            Authentication authentication = tokenManager.getAuthenticationByToken(reqToken);
            // 未获取到用户信息
            if (authentication == null) {
                // 继续执行后续过滤器
                chain.doFilter(request, response);
                return;
            }

            // 创建空的SecurityContext
            SecurityContext context = this.securityContextHolderStrategy.createEmptyContext();
            // 设置认证信息
            context.setAuthentication(authentication);
            // 保存到SecurityContextHolder
            this.securityContextHolderStrategy.setContext(context);
            chain.doFilter(request, response);
            // filter执行完成后,移除已执行标记
            request.removeAttribute(FILTER_APPLIED);
        }
    }
}
