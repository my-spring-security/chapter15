package com.lh0811.starter.security;

import com.lh0811.starter.security.common.filter.TokenSecurityContextHolderFilter;
import com.lh0811.starter.security.common.handler.JsonAccessDeniedHandler;
import com.lh0811.starter.security.common.handler.JsonAuthenticationEntryPoint;
import com.lh0811.starter.security.common.handler.JsonLogoutSuccessHandler;
import com.lh0811.starter.security.component.BaseAbstractAuthenticationProcessingFilter;
import com.lh0811.starter.security.component.BaseAuthenticationConfig;
import com.lh0811.starter.security.component.BaseAuthenticationProvider;
import com.lh0811.starter.security.component.token.TokenManager;
import com.lh0811.starter.security.component.token.impl.DefaultTokenManager;
import com.lh0811.starter.security.config.LH0811SpringSecurityProperties;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.core.GrantedAuthorityDefaults;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.context.SecurityContextHolderFilter;

import java.util.List;

@EnableConfigurationProperties({LH0811SpringSecurityProperties.class})
public class LH0811StarterSpringSecurityConfigure {


    private TokenManager tokenManager;

    private List<BaseAuthenticationProvider> providerList;

    private LH0811SpringSecurityProperties lh0811SpringSecurityProperties;


    public LH0811StarterSpringSecurityConfigure(@Lazy List<BaseAuthenticationProvider> providerList,@Lazy TokenManager tokenManager,@Lazy LH0811SpringSecurityProperties lh0811SpringSecurityProperties) {
        this.providerList = providerList;
        this.tokenManager = tokenManager;
        this.lh0811SpringSecurityProperties = lh0811SpringSecurityProperties;
    }

    @Bean
    @ConditionalOnMissingBean(TokenManager.class)
    public TokenManager tokenManager() {
        return new DefaultTokenManager();
    }



    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public GrantedAuthorityDefaults grantedAuthorityDefaults() {
        return new GrantedAuthorityDefaults(lh0811SpringSecurityProperties.getRolePrefix());
    }


    @Bean
    @ConditionalOnMissingBean(SecurityFilterChain.class)
    SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http) throws Exception {
        // 所有请求都需要认证后访问
        http.authorizeHttpRequests((authorizeHttpRequests) -> authorizeHttpRequests.anyRequest().authenticated());
        // 禁用默认的formLogin
        http.formLogin((formLogin) -> formLogin.disable());
        // 禁用httpBasic
        http.httpBasic((httpBasic) -> httpBasic.disable());
        // 禁用csrf
        http.csrf((csrf) -> csrf.disable());

        // 将token解析认证信息的Filter 添加到SecurityContextHolderFilter之后
        http.addFilterAfter(new TokenSecurityContextHolderFilter(tokenManager,lh0811SpringSecurityProperties), SecurityContextHolderFilter.class);

        // 启用自定义认证流程
        BaseAbstractAuthenticationProcessingFilter filter = new BaseAbstractAuthenticationProcessingFilter(lh0811SpringSecurityProperties);
        http.apply(new BaseAuthenticationConfig<>(filter,providerList,lh0811SpringSecurityProperties));

        // 注销
        http.logout((logout) -> logout
                .logoutUrl(lh0811SpringSecurityProperties.getLogoutProcessingUrl())
                .logoutSuccessHandler(new JsonLogoutSuccessHandler(tokenManager)));
        // 异常
        http.exceptionHandling((exception) -> exception
                // 用户未认证异常处理。 未认证的情况下默认是LoginUrlAuthenticationEntryPoint 跳转到 请求用户认证的页面
                .authenticationEntryPoint(new JsonAuthenticationEntryPoint())
                // 鉴权失败异常处理。
                .accessDeniedHandler(new JsonAccessDeniedHandler())
        );
        return http.build();
    }

}
