package com.lh0811.starter.security.test.repository.dao;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lh0811.starter.security.test.repository.entity.Users;

/**
 * 
 *
 * @author 
 * @email 
 * @date 2023-08-09 16:46:50
 */
public interface UsersDao extends IService<Users> {

}
