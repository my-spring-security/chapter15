package com.lh0811.starter.security.test.repository.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * 三方认证平台用户关联关系
 * 
 * @author 
 * @email 
 * @date 2023-08-09 16:46:50
 */
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@TableName("third_auth_user_rel")
public class ThirdAuthUserRel implements Serializable {

	@TableId(type= IdType.NONE)
	private Long id;
	// 平台类型 eg：gitee")
	private String thirdType;
	// 三方平台id")
	private String thirdId;
	// 本地用户手机号")
	private Long localUserId;

}
