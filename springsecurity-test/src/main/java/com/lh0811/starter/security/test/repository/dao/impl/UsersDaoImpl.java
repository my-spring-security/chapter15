package com.lh0811.starter.security.test.repository.dao.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lh0811.starter.security.test.repository.dao.UsersDao;
import com.lh0811.starter.security.test.repository.dao.mapper.UsersMapper;
import com.lh0811.starter.security.test.repository.entity.Users;
import org.springframework.stereotype.Service;

/**
 * 
 *
 * @author 
 * @email 
 * @date 2023-08-09 16:46:50
 */
@Service
public class UsersDaoImpl extends ServiceImpl<UsersMapper, Users> implements UsersDao {

}
