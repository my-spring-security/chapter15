package com.lh0811.starter.security.test.auth.usernamepwd;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.lh0811.starter.security.common.modal.AuthenticationSuccessToken;
import com.lh0811.starter.security.component.BaseAuthenticationProvider;
import com.lh0811.starter.security.component.token.TokenManager;
import com.lh0811.starter.security.test.modal.MyUser;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.stream.Collectors;

@Slf4j
@Component
public class MyUsernamePasswordAuthenticationProvider implements BaseAuthenticationProvider {

    @Resource
    private MyUserDetailsService myUserDetailsService;

    @Resource
    private PasswordEncoder passwordEncoder;

    @Resource
    private TokenManager tokenManager;


    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        // 获取未认证的token
        MyUsernamePasswordAuthenticationToken authenticationToken = (MyUsernamePasswordAuthenticationToken) authentication;
        String username = authenticationToken.getPrincipal();// 获取凭证也就是用户的手机号
        String password = authenticationToken.getCredentials(); // 获取输入的验证码
        // 获取用户信息
        MyUser myUser = (MyUser) myUserDetailsService.loadUserByUsername(username);
        // 验证密码是否匹配
        if (!passwordEncoder.matches(password, myUser.getUsers().getPassword())) {
            throw new BadCredentialsException("用户名或密码错误");
        }
        // 获取已认证的token
        AuthenticationSuccessToken authenticated = AuthenticationSuccessToken.buildForSystemUser(myUser);
        try {
            String token = tokenManager.generateToken(authenticated);
            ((MyUser) authenticated.getDetails()).setToken(token);
            return authenticated;
        } catch (Exception e) {
            throw new BadCredentialsException("Token口令生成失败");
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return MyUsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
    }

    @Override
    public String loginType() {
        return "my_username_login";
    }

    @Override
    public Authentication genAuthenticationFromRequest(HttpServletRequest request) throws IOException {
        String bodyStr = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
        String username;
        String password;
        try {
            JSONObject bodyJson = JSON.parseObject(bodyStr);
            username = bodyJson.getString("username");
            password = bodyJson.getString("password");
            if (StringUtils.isBlank(username)) {
                throw new BadCredentialsException("用户名未上传");
            }
            if (StringUtils.isBlank(password)) {
                throw new BadCredentialsException("密码未上传");
            }
        } catch (Exception e) {
            log.error("请求参数格式不合法", e);
            throw new BadCredentialsException("请求参数格式不合法");
        }
        return MyUsernamePasswordAuthenticationToken.MyUsernamePasswordAuthenticationTokenUnAuthenticated(username, password);
    }
}
