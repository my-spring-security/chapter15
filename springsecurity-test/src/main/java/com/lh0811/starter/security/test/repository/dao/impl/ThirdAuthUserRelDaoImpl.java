package com.lh0811.starter.security.test.repository.dao.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lh0811.starter.security.test.repository.dao.ThirdAuthUserRelDao;
import com.lh0811.starter.security.test.repository.dao.mapper.ThirdAuthUserRelMapper;
import com.lh0811.starter.security.test.repository.entity.ThirdAuthUserRel;
import org.springframework.stereotype.Service;

/**
 * 三方认证平台用户关联关系
 *
 * @author 
 * @email 
 * @date 2023-08-09 16:46:50
 */
@Service
public class ThirdAuthUserRelDaoImpl extends ServiceImpl<ThirdAuthUserRelMapper, ThirdAuthUserRel> implements ThirdAuthUserRelDao {

}
