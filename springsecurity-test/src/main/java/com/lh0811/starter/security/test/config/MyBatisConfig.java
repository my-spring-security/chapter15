package com.lh0811.starter.security.test.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author lh0811
 * @date 2022/6/20
 */
@Configuration
@MapperScan("com.lh0811.starter.security.test.repository.dao.mapper")
public class MyBatisConfig {

}
