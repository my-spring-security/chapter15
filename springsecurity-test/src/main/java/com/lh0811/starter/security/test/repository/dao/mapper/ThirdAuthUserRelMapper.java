package com.lh0811.starter.security.test.repository.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lh0811.starter.security.test.repository.entity.ThirdAuthUserRel;

/**
 * 三方认证平台用户关联关系
 *
 * @author 
 * @email 
 * @date 2023-08-09 16:46:50
 */
public interface ThirdAuthUserRelMapper extends BaseMapper<ThirdAuthUserRel> {

}

