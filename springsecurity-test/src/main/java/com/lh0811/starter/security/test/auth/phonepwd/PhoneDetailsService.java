package com.lh0811.starter.security.test.auth.phonepwd;

import com.lh0811.starter.security.test.modal.MyUser;
import com.lh0811.starter.security.test.repository.dao.AuthoritiesDao;
import com.lh0811.starter.security.test.repository.dao.UsersDao;
import com.lh0811.starter.security.test.repository.entity.Authorities;
import com.lh0811.starter.security.test.repository.entity.Users;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Slf4j
@Service
public class PhoneDetailsService implements UserDetailsService {

    @Resource
    private UsersDao usersDao;

    @Resource
    private AuthoritiesDao authoritiesDao;

    @Override
    public UserDetails loadUserByUsername(String phone) throws UsernameNotFoundException {
        // 验证用户主信息
        if (StringUtils.isBlank(phone)) {
            throw new BadCredentialsException("未上传手机号");
        }
        Users users = usersDao.lambdaQuery().eq(Users::getPhone, phone).one();
        if (users == null) {
            throw new BadCredentialsException("手机号不存在");
        }
        // 封装自定义用户信息
        MyUser myUser = MyUser.createByUsers(users);
        // 补充权限信息
        List<Authorities> list = authoritiesDao.lambdaQuery().eq(Authorities::getUserId,users.getId()).list();
        if (!CollectionUtils.isEmpty(list)) {
            for (Authorities authorities : list) {
                myUser.getAuthorities().add(new SimpleGrantedAuthority(authorities.getAuthority()));
            }
        }
        // 返回结果
        return myUser;
    }

}
