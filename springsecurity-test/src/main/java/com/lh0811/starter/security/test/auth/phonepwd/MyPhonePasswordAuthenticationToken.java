package com.lh0811.starter.security.test.auth.phonepwd;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class MyPhonePasswordAuthenticationToken extends AbstractAuthenticationToken {

    private String phone;
    private String password;

    private MyPhonePasswordAuthenticationToken(Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
    }

    // 未认证时
    public static MyPhonePasswordAuthenticationToken buildUnAuthenticated(String phone, String password) {
        MyPhonePasswordAuthenticationToken token = new MyPhonePasswordAuthenticationToken(null);
        token.phone = phone;
        token.password = password;
        token.setAuthenticated(Boolean.FALSE);
        return token;
    }

    @Override
    public String getCredentials() {
        return this.password;
    }

    @Override
    public String getPrincipal() {
        return this.phone;
    }


}