package com.lh0811.starter.security.test.repository.dao;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lh0811.starter.security.test.repository.entity.ThirdAuthUserRel;

/**
 * 三方认证平台用户关联关系
 *
 * @author 
 * @email 
 * @date 2023-08-09 16:46:50
 */
public interface ThirdAuthUserRelDao extends IService<ThirdAuthUserRel> {

}
