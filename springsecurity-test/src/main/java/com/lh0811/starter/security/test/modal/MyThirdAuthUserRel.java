package com.lh0811.starter.security.test.modal;

import com.lh0811.starter.security.oauth2.login.handler.OAuth2AuthenticationTokenAndLocalUserRel;
import com.lh0811.starter.security.test.repository.entity.ThirdAuthUserRel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MyThirdAuthUserRel implements OAuth2AuthenticationTokenAndLocalUserRel {

    private ThirdAuthUserRel thirdAuthUserRel;

    public static MyThirdAuthUserRel build(ThirdAuthUserRel thirdAuthUserRel) {
        MyThirdAuthUserRel myThirdAuthUserRel = new MyThirdAuthUserRel();
        myThirdAuthUserRel.setThirdAuthUserRel(thirdAuthUserRel);
        return myThirdAuthUserRel;
    }

    @Override
    public String getLocalUserId() {
        return String.valueOf(thirdAuthUserRel.getLocalUserId());
    }

    @Override
    public String getRegistrationId() {
        return thirdAuthUserRel.getThirdType();
    }

    @Override
    public String getThirdUserId() {
        return thirdAuthUserRel.getThirdId();
    }

    @Override
    public Boolean getBuildingFlag() {
        return thirdAuthUserRel.getLocalUserId() != null;
    }
}
