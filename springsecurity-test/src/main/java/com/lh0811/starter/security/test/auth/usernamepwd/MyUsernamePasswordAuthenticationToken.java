package com.lh0811.starter.security.test.auth.usernamepwd;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class MyUsernamePasswordAuthenticationToken extends AbstractAuthenticationToken {

    private String username;
    private String password;

    private MyUsernamePasswordAuthenticationToken(Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
    }

    // 未认证时
    public static MyUsernamePasswordAuthenticationToken MyUsernamePasswordAuthenticationTokenUnAuthenticated(String username, String password) {
        MyUsernamePasswordAuthenticationToken token = new MyUsernamePasswordAuthenticationToken(null);
        token.username = username;
        token.password = password;
        token.setAuthenticated(Boolean.FALSE);
        return token;
    }


    @Override
    public String getCredentials() {
        return this.password;
    }

    @Override
    public String getPrincipal() {
        return this.username;
    }


}