package com.lh0811.starter.security.test.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class HelloController {

    @GetMapping("/hello")
    public String hello() {
        return "Hello SpringSecurity！！!";
    }

    @PreAuthorize("hasRole('MYROLE_ADMIN')")
    @GetMapping("/admin")
    public String admin() {
        return "THIS IS ADMIN";
    }

    // hasRole('MYROLE_WORKER') 和 hasRole('WORKER') 这两种都可以，但是数据库中维护必须带着前缀否则不能识别为角色
    // @PreAuthorize("hasRole('MYROLE_WORKER')")
    @PreAuthorize("hasRole('WORKER')") // 默认会再前面追加前缀，匹配authorities中维护的信息
    @GetMapping("/worker")
    public String worker() {
        return "THIS IS WORKER";
    }

    @PreAuthorize("hasAuthority('user:add')")
    @GetMapping("/user_add")
    public String userAdd() {
        return "THIS IS user add";
    }

    @GetMapping("/err")
    public String err() {
        int a = 1/0;
        return "Success";
    }

    @GetMapping("/user_info")
    public Object getCurrentUser() throws Exception {
        return SecurityContextHolder.getContext().getAuthentication();
    }
}
