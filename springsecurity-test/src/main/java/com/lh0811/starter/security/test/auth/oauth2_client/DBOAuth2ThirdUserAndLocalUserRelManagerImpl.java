package com.lh0811.starter.security.test.auth.oauth2_client;

import com.lh0811.starter.security.common.modal.SystemUser;
import com.lh0811.starter.security.oauth2.login.component.OAuth2ThirdUserAndLocalUserRelManager;
import com.lh0811.starter.security.oauth2.login.modal.BuildingUserParam;
import com.lh0811.starter.security.oauth2.login.modal.BuildingUserVo;
import com.lh0811.starter.security.test.modal.MyUser;
import com.lh0811.starter.security.test.repository.dao.AuthoritiesDao;
import com.lh0811.starter.security.test.repository.dao.ThirdAuthUserRelDao;
import com.lh0811.starter.security.test.repository.dao.UsersDao;
import com.lh0811.starter.security.test.repository.entity.Authorities;
import com.lh0811.starter.security.test.repository.entity.ThirdAuthUserRel;
import com.lh0811.starter.security.test.repository.entity.Users;
import com.lh0811.starter.web.exception.CommonException;
import com.lh0811.starter.web.response.ServerResponse;
import com.lh0811.starter.web.util.IdWorker;
import jakarta.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class DBOAuth2ThirdUserAndLocalUserRelManagerImpl implements OAuth2ThirdUserAndLocalUserRelManager {

    @Resource
    private IdWorker idWorker;

    @Resource
    private ThirdAuthUserRelDao thirdAuthUserRelDao;

    @Resource
    private UsersDao usersDao;

    @Resource
    private AuthoritiesDao authoritiesDao;

    @Resource
    private PasswordEncoder passwordEncoder;

    @Override
    public SystemUser loadUser(String registrationId, String thirdId) throws Exception {
        ThirdAuthUserRel userRel = thirdAuthUserRelDao.lambdaQuery()
                .eq(ThirdAuthUserRel::getThirdType, registrationId)
                .eq(ThirdAuthUserRel::getThirdId, thirdId)
                .one();
        if (userRel == null) return null;
        Users users = usersDao.lambdaQuery().eq(Users::getId, Long.valueOf(userRel.getLocalUserId())).one();
        if (users == null) return null;
        List<Authorities> authorities = authoritiesDao.lambdaQuery().eq(Authorities::getUserId, users.getId()).list();
        List<SimpleGrantedAuthority> authorityList = authorities.stream().map(ele -> new SimpleGrantedAuthority(ele.getAuthority())).collect(Collectors.toList());
        MyUser myUser = MyUser.createByUsers(users);
        myUser.getAuthorities().addAll(authorityList);
        return myUser;
    }

    @Override
    public BuildingUserVo buildingUser(BuildingUserParam param) throws Exception {
        ThirdAuthUserRel userRel = thirdAuthUserRelDao.lambdaQuery()
                .eq(ThirdAuthUserRel::getThirdType, param.getRegistrationId())
                .eq(ThirdAuthUserRel::getThirdId, param.getThirdUserId())
                .one();
        if (userRel != null) {
            Users users = usersDao.lambdaQuery().eq(Users::getId, Long.valueOf(userRel.getLocalUserId())).one();
            List<Authorities> authorities = authoritiesDao.lambdaQuery().eq(Authorities::getUserId, users.getId()).list();
            List<SimpleGrantedAuthority> authorityList = authorities.stream().map(ele -> new SimpleGrantedAuthority(ele.getAuthority())).collect(Collectors.toList());
            MyUser myUser = MyUser.createByUsers(users);
            myUser.getAuthorities().addAll(authorityList);
            return new BuildingUserVo("1",myUser);
        }else {
            if (!StringUtils.equals(param.getPwd(),param.getRepwd())) {
                throw CommonException.create(ServerResponse.createByError("两次输入密码不一致"));
            }
            Users users = new Users();
            users.setId(idWorker.nextId());
            users.setUsername("");
            users.setPhone(param.getPhone());
            users.setPassword(passwordEncoder.encode(param.getPwd()));
            users.setEnabled(Boolean.TRUE);
            usersDao.save(users);
            ThirdAuthUserRel newRel = new ThirdAuthUserRel();
            newRel.setId(idWorker.nextId());
            newRel.setThirdType(param.getRegistrationId());
            newRel.setThirdId(param.getThirdUserId());
            newRel.setLocalUserId(users.getId());
            thirdAuthUserRelDao.save(newRel);
            MyUser myUser = MyUser.createByUsers(users);
            return new BuildingUserVo("0",myUser);
        }
    }
}
