package com.lh0811.starter.security.test.modal;

import com.lh0811.starter.security.common.modal.SystemUser;
import com.lh0811.starter.security.test.repository.entity.Users;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MyUser implements SystemUser {

    private Users users;

    // 用户认证成功后颁发的token
    private String token;

    private List<GrantedAuthority> authorities = new ArrayList<>();


    public static MyUser createByUsers(Users users) {
        MyUser myUser = new MyUser();
        myUser.users = users;
        return myUser;
    }
    public static MyUser createByUsers(Users users,List<GrantedAuthority> authorities) {
        MyUser myUser = new MyUser();
        myUser.users = users;
        myUser.authorities = authorities;
        return myUser;
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return this.authorities;
    }

    @Override
    public String getPassword() {
        return users.getPassword();
    }

    @Override
    public String getUsername() {
        return users.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return this.users.getEnabled();
    }

    @Override
    public String getUserId() {
        return String.valueOf(this.users.getId());
    }
}