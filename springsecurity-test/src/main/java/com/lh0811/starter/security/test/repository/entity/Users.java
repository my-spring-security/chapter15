package com.lh0811.starter.security.test.repository.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * 
 * 
 * @author 
 * @email 
 * @date 2023-08-09 16:46:50
 */
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@TableName("users")
public class Users implements Serializable {

	@TableId(type= IdType.NONE)
	private Long id;
	private String username;
	private String phone;
	private String password;
	private Boolean enabled;

}
