package com.lh0811.starter.security.test.repository.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lh0811.starter.security.test.repository.entity.Users;

/**
 * 
 *
 * @author 
 * @email 
 * @date 2023-08-09 16:46:50
 */
public interface UsersMapper extends BaseMapper<Users> {

}

