package com.lh0811.starter.security.test.repository.dao.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lh0811.starter.security.test.repository.dao.AuthoritiesDao;
import com.lh0811.starter.security.test.repository.dao.mapper.AuthoritiesMapper;
import com.lh0811.starter.security.test.repository.entity.Authorities;
import org.springframework.stereotype.Service;

/**
 * 
 *
 * @author 
 * @email 
 * @date 2023-08-09 16:46:50
 */
@Service
public class AuthoritiesDaoImpl extends ServiceImpl<AuthoritiesMapper, Authorities> implements AuthoritiesDao {

}
