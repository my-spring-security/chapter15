package com.lh0811.starter.web.exception;


import com.lh0811.starter.web.response.ResponseCode;
import com.lh0811.starter.web.response.ServerResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

/**
 * 自定义异常处理
 */
@Slf4j
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CommonException extends Exception {

    private Throwable error;

    private ServerResponse serverResponse;

    public static CommonException create(ServerResponse responseEntity) {
        CommonException exception = new CommonException();
        exception.serverResponse = responseEntity;
        return exception;
    }

    public static CommonException create(Throwable error, ServerResponse responseEntity) {
        CommonException exception = new CommonException();
        exception.serverResponse = responseEntity;
        exception.error = error;
        return exception;
    }

    public static CommonException create(ResponseCode responseCode) {
        CommonException exception = new CommonException();
        ServerResponse serverResponse = new ServerResponse();
        serverResponse.setStatus(responseCode.getCode());
        serverResponse.setMsg(responseCode.getDesc());
        exception.serverResponse = serverResponse;
        return exception;
    }

    public static CommonException create(ResponseCode responseCode, String... str) {
        CommonException exception = new CommonException();
        ServerResponse serverResponse = new ServerResponse();
        serverResponse.setStatus(responseCode.getCode());
        serverResponse.setMsg(responseCode.getDesc());
        exception.serverResponse = serverResponse;

        int count = StringUtils.countMatches(responseCode.getDesc(), "%s");
        if (count > 0) {
            if (str.length > 0) {
                String[] strings = Arrays.copyOf(str, count);
                String format = String.format(exception.serverResponse.getMsg(), strings);
                exception.serverResponse.setMsg(format);
            }
        }

        return exception;
    }

}
