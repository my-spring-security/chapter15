package com.lh0811.starter.web;

import com.lh0811.starter.web.util.IdWorker;
import org.springframework.context.annotation.Bean;

public class LH0811StarterWebAutoConfigure {
    @Bean
    public IdWorker idWorker() {
        return new IdWorker();
    }
}
