package com.lh0811.starter.web.util;

import com.google.common.collect.Lists;
import org.springframework.beans.BeanUtils;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @author lh0811
 * @date 2022/5/11
 */
public class BeanHelper {

    public static <S,T>  T copyBean(S source, Class<T> target) throws Exception {
        if (source == null) {
            return null;
        }
        T t = target.newInstance();
        BeanUtils.copyProperties(source,t);
        return t;
    }

    public static <S,T>  List<T> copyList(List<S> sources, Class<T> target) throws Exception {
        if (CollectionUtils.isEmpty(sources)) {
            return Lists.newArrayList();
        }
        List<T> tLists = Lists.newArrayList();
        for (S source : sources) {
            T t = target.newInstance();
            BeanUtils.copyProperties(source,t);
            tLists.add(t);
        }
        return tLists;
    }

}
