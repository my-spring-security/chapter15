package com.lh0811.starter.web.exception;

import com.lh0811.starter.web.response.ResponseCode;
import com.lh0811.starter.web.response.ServerResponse;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.lang.reflect.UndeclaredThrowableException;


@Slf4j
@ControllerAdvice
public class CommonExceptionAdvice {

    // 自定义异常处理
    @ResponseBody
    @ExceptionHandler(value = {CommonException.class})
    public ServerResponse myError(CommonException e) {
        return e.getServerResponse();
    }

    // 全局异常
    @ResponseBody
    @ExceptionHandler(value = {Exception.class})
    public ServerResponse globalError(HttpServletRequest request, Exception e) {
        e.printStackTrace();
        if (StringUtils.equals(e.getClass().getName(),"org.springframework.security.access.AccessDeniedException") ) {
            return ServerResponse.createByError(ResponseCode.ERR_AUTH.getCode(),"当前用户没有权限访问该资源,请联系管理员申请资源权限。");
        }
        if (StringUtils.equals(e.getClass().getName(),"org.springframework.security.core.AuthenticationException") ) {
            return ServerResponse.createByError(ResponseCode.ERR_AUTH.getCode(),"未获取到用户信息，请登录后重试。");
        }
        // 返回前端提示信息
        return ServerResponse.createByError(500, "未知错误！");
    }



    @ResponseBody
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ServerResponse throwCustomException(MethodArgumentNotValidException methodArgumentNotValidException) {
        // 这个不假sentinel的异常统计 这个不是系统异常
        return ServerResponse.createByError(2,"非法参数", methodArgumentNotValidException.getBindingResult().getFieldError().getDefaultMessage());
    }

    @ResponseBody
    @ExceptionHandler(value = {UndeclaredThrowableException.class})
    public ServerResponse globalUndeclaredThrowableException(HttpServletRequest request, Exception e) {
        // 输出控制台打印日志
        if (((UndeclaredThrowableException) e).getUndeclaredThrowable() instanceof CommonException) {
            return myError((CommonException) ((UndeclaredThrowableException) e).getUndeclaredThrowable());
        } else {
            return globalError(request, e);
        }
    }





}
